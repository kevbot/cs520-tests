# CS520 Test Files

> Note: There is **no** assignment source code here. This just stores test cases for the projects.
> 
> If you pass all the test cases here DOES NOT mean you will get a 100 on Mimir

## Usage

Download the test cases for the lab you are working on and run the test script. A makefile may be included but is not needed to run the test cases. If there are any other requirements, refer to the README file found with the test cases.

Usually it is as simple as downloading the test files and running `./test.sh` in the folder with your executables. This requires the file layout to exactly match the directory layout, with test.sh being next to the test folder.

# Downloads

These links are for convenience and may not be up to date.

- [Program1](https://gitlab.com/kevbot/cs520-tests/-/archive/master/cs520-tests-master.zip?path=Program1)
- [Program2](https://gitlab.com/kevbot/cs520-tests/-/archive/master/cs520-tests-master.zip?path=Program2)


If you would like to help, let me know.

# Issues

If you find a problem, create an issue. DO NOT use the issues for asking for program help.
