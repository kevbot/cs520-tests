#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <math.h>

int32_t f2i(uint32_t in);
uint32_t i2f(int32_t in);
uint64_t s2d(uint32_t in);

#define HIDE_ALL 0
#define SHOW_ERR 1
#define SHOW_ALL 2
#define COLOR    4

#define FAIL "[FAIL] "
#define PASS "[PASS] "
#define FAIL_C "\033[0;31m" FAIL "\033[0m"
#define PASS_C "\033[0;32m" PASS "\033[0m"

union fint { // float int
	float f;
	uint32_t u;
	int32_t i;
};

union dong { // double long
	double d;
	uint64_t l;
};

unsigned long f2i_compare(float num, unsigned char mode) {
	union fint input;
	union fint expected;
	union fint actual;
	input.f = num;
	expected.i = (int32_t)num;
	actual.i = f2i(input.u); // pass in float interpreted as uint32_t
	if ((actual.u != expected.u && mode & SHOW_ERR) || (mode & SHOW_ALL)) {
		if (actual.u != expected.u) {
			if (mode & COLOR) {
				fprintf(stderr, FAIL_C);
			} else {
				fprintf(stderr, FAIL);
			}
		} else {
			if (mode & COLOR) {
				fprintf(stderr, PASS_C);
			} else {
				fprintf(stderr, PASS);
			}

		}
		fprintf(stderr, "Input: %f\tExpected: %d\tActual: %d\n       Hex Input: %" PRIx32 "\tExpected: %" PRIx32 "\tActual: %" PRIx32 "\n", num, expected.i, actual.i, input.u, expected.u, actual.u);
	}
	if (actual.u != expected.u) {
		return 1;
	}
	return 0;
}

unsigned long i2f_compare(int32_t num, unsigned char mode) {
	union fint input;
	union fint expected;
	union fint actual;
	input.i = num;
	expected.f = (float)num;
	actual.u = i2f(num);
	if ((actual.u != expected.u && mode & SHOW_ERR) || (mode & SHOW_ALL)) {
		if (actual.u != expected.u) {
			if (mode & COLOR) {
				fprintf(stderr, FAIL_C);
			} else {
				fprintf(stderr, FAIL);
			}
		} else {
			if (mode & COLOR) {
				fprintf(stderr, PASS_C);
			} else {
				fprintf(stderr, PASS);
			}

		}
		fprintf(stderr, "Input: %d\tExpected: %f\tActual: %f\n       Hex Input: %" PRIx32 "\tExpected: %" PRIx32 "\tActual: %" PRIx32 "\n", num, expected.f, actual.f, input.u, expected.u, actual.u);
	}
	if (actual.u != expected.u) {
		return 1;
	}
	return 0;
}

unsigned long s2d_compare(float num, unsigned char mode) {
	union fint input;
	union dong actual;
	union dong expected;
	input.f = num;
	expected.d = (double)num;
	actual.l = s2d(input.u); // pass in float interpreted as uint32_t
	if (( actual.l != expected.l && mode & SHOW_ERR) || (mode & SHOW_ALL)) {
		if (actual.l != expected.l) {
			if (mode & COLOR) {
				fprintf(stderr, FAIL_C);
			} else {
				fprintf(stderr, FAIL);
			}
		} else {
			if (mode & COLOR) {
				fprintf(stderr, PASS_C);
			} else {
				fprintf(stderr, PASS);
			}

		}
		fprintf(stderr, "Input: %f\tExpected: %f\tActual: %f\n       Hex Input: %" PRIx32 "\tExpected: %" PRIx64 "\tActual: %" PRIx64 "\n", num, expected.d, actual.d, input.u, expected.l, actual.l);
	}
	if (actual.l != expected.l) {
		return 1;
	}
	return 0;
}

unsigned long f2i_range(float low, float high, float delta, unsigned char mode) {
	unsigned int total = 0;
	unsigned int fail = 0;
	fprintf(stderr, "Float to Int Range Test\n");
	for (float i = low; i < high; i+= delta) {
		fail += f2i_compare(i, mode);
		total++;
	}
	if (fail != 0) {
		fprintf(stderr, "Errors: %d / %d (%.2f%%)\n", fail, total, 100.0 * fail / (float) total);
	} else {
		fprintf(stderr, "Passed 100%%\n");
	}
	return fail;
}

unsigned long f2i_infinities(unsigned char mode) {
	fprintf(stderr, "Float Infinity to Int\n");
	unsigned int fail = f2i_compare(1.0/0.0, mode);
	fail += f2i_compare(-1.0/0.0, mode);
	if (fail != 0) {
		fprintf(stderr, "Errors: %d / %d (%.2f%%)\n", fail, 2, (float) fail / 2);
	} else {
		fprintf(stderr, "Passed 100%%\n");
	}
	return fail;
}

unsigned long f2i_zeros(unsigned char mode) {
	fprintf(stderr, "Float near 0 to Int\n");
	unsigned int fail = f2i_compare(0.0, mode);
	fail += f2i_compare(-0.0, mode);
	fail += f2i_compare(0.0000000003, mode);
	fail += f2i_compare(-0.0000000003, mode);
	if (fail != 0) {
		fprintf(stderr, "Errors: %d / %d (%.2f%%)\n", fail, 4, (float) fail / 4);
	} else {
		fprintf(stderr, "Passed 100%%\n");
	}
	return fail;
}

unsigned long f2i_large(unsigned char mode) {
	fprintf(stderr, "Float Large to Int\n");
	unsigned int fail = f2i_compare(10000000000000.0, mode);
	fail += f2i_compare(-10000000000000.0, mode);
	fail += f2i_compare(2147483648.0, mode);
	fail += f2i_compare(-2147483648.0, mode);
	fail += f2i_compare(2147483520.0, mode);
	fail += f2i_compare(-2147483520.0, mode);
	if (fail != 0) {
		fprintf(stderr, "Errors: %u / %u (%.2f%%)\n", fail, 6, (float) fail / 6);
	} else {
		fprintf(stderr, "Passed 100%%\n");
	}
	return fail;
}

unsigned long f2i_full_test(unsigned char mode) {
	union fint num;
	num.u = 0;
	unsigned int fail = 0;
	fail += f2i_compare(num.i, mode);
	num.u = 0xFFFFFFFF;
	for (; num.u != 0; num.u--) {
		fail += f2i_compare(num.i, mode);
	}
	return fail;
}

unsigned long i2f_range(int32_t low, int32_t high, int32_t delta, unsigned char mode) {
	unsigned int total = 0;
	unsigned int fail = 0;
	fprintf(stderr, "Int to Float Range Test\n");
	for (int32_t i = low; i < high; i+= delta) {
		fail += i2f_compare(i, mode);
		total++;
	}
	if (fail != 0) {
		fprintf(stderr, "Errors: %d / %d (%.2f%%)\n", fail, total, 100.0 * fail / (float) total);
	} else {
		fprintf(stderr, "Passed 100%%\n");
	}
	return fail;
}

unsigned long i2f_truncate(unsigned char mode) {
	unsigned int fail = 0;
	fprintf(stderr, "Int to Float Truncate Test\n");
	fail += i2f_compare(0x34567800, mode);
	fail += i2f_compare(0x34567801, mode);
	fail += i2f_compare(0x34567890, mode);
	fail += i2f_compare(0x34567891, mode);
	if (fail != 0) {
		fprintf(stderr, "Errors: %d / %d (%.2f%%)\n", fail, 4, 100.0 * fail / (float) 4);
	} else {
		fprintf(stderr, "Passed 100%%\n");
	}
	return fail;
}

unsigned long i2f_round_to_even(unsigned char mode) {
	unsigned int fail = 0;
	fprintf(stderr, "Int to Float Round to Even Test\n");
	fail += i2f_compare(0x345678A0, mode);
	fail += i2f_compare(0x345678E0, mode);
	if (fail != 0) {
		fprintf(stderr, "Errors: %d / %d (%.2f%%)\n", fail, 2, 100.0 * fail / (float) 2);
	} else {
		fprintf(stderr, "Passed 100%%\n");
	}
	return fail;
}

unsigned long i2f_add_one(unsigned char mode) {
	unsigned int fail = 0;
	fprintf(stderr, "Int to Float Add One Test\n");
	fail += i2f_compare(0x345678A1, mode);
	fail += i2f_compare(0x345678B0, mode);
	fail += i2f_compare(0x345678B1, mode);
	if (fail != 0) {
		fprintf(stderr, "Errors: %d / %d (%.2f%%)\n", fail, 3, 100.0 * fail / (float) 3);
	} else {
		fprintf(stderr, "Passed 100%%\n");
	}
	return fail;
}

unsigned long i2f_full_test(unsigned char mode) {
	union fint num;
	num.u = 0;
	unsigned int fail = 0;
	fail += i2f_compare(num.i, mode);
	num.u = 0xFFFFFFFF;
	for (; num.u != 0; num.u--) {
		fail += i2f_compare(num.i, mode);
	}
	return fail;
}

unsigned long s2d_range(float low, float high, float delta, unsigned char mode) {
	unsigned int total = 0;
	unsigned int fail = 0;
	fprintf(stderr, "Single to Double Range Test\n");
	for (float i = low; i < high; i+= delta) {
		fail += s2d_compare(i, mode);
		total++;
	}
	if (fail != 0) {
		fprintf(stderr, "Errors: %d / %d (%.2f%%)\n", fail, total, 100.0 * fail / (float) total);
	} else {
		fprintf(stderr, "Passed 100%%\n");
	}
	return fail;
}

unsigned long s2d_infinities(unsigned char mode) {
	union fint num;
	unsigned int fail = 0;
	num.u = 0x7F800000;
	fail += s2d_compare(num.f, mode);
	num.u = 0xFF800000;
	fail += s2d_compare(num.f, mode);
	return fail;
}

unsigned long s2d_nan(unsigned char mode) {
	union fint num;
	unsigned int fail = 0;
	num.u = 0x7FBFFFFF;
	fail += s2d_compare(num.f, mode);
	num.u = 0x7FBF00FF;
	fail += s2d_compare(num.f, mode);
	num.u = 0x7FBF00FF;
	fail += s2d_compare(num.f, mode);
	num.u = 0x7FBF00AF;
	fail += s2d_compare(num.f, mode);
	num.u = 0x7FBF001;
	fail += s2d_compare(num.f, mode);
	num.u = 0xFFBFFFFF;
	fail += s2d_compare(num.f, mode);
	num.u = 0xFFBF00FF;
	fail += s2d_compare(num.f, mode);
	num.u = 0xFFBF00FF;
	fail += s2d_compare(num.f, mode);
	num.u = 0xFFBF00AF;
	fail += s2d_compare(num.f, mode);
	num.u = 0xFFBF001;
	fail += s2d_compare(num.f, mode);
	return fail;
}

unsigned long s2d_denormalized(unsigned char mode) {
	union fint num;
	unsigned int fail = 0;
	num.u = 0x807795FD;
	fail += s2d_compare(num.f, mode);
	num.u = 0x803fd246;
	fail += s2d_compare(num.f, mode);
	num.u = 0x007795FD;
	fail += s2d_compare(num.f, mode);
	num.u = 0x003fd246;
	fail += s2d_compare(num.f, mode);
	
	return fail;
}

unsigned long s2d_full_test(unsigned char mode) {
	union fint num;
	num.u = 0;
	unsigned int fail = 0;
	fail += s2d_compare(num.f, mode);
	num.u = 0xFFFFFFFF;
	for (; num.u != 0; num.u--) {
		fail += s2d_compare(num.f, mode);
	}
	return fail;
}

unsigned int combine_tests(unsigned char tests) {
	unsigned int combined = 0;
	for (unsigned int i = 0; i < 8; i++) {
		if (1<<i & tests) {
			combined |= 1 << (i * 3);
		}
	}
	return combined;
}

// TODO: Fix and update to properly change tests
unsigned char parse_flags(int argc, char *argv[], unsigned int* tests) {
	unsigned char mode = SHOW_ERR;
	unsigned char run_tests = 0; // bits: 3=full 2=special 1=range 0=basic
	unsigned char to_test = 0; // bits: 2=s2d 1=i2f 0=f2i
	unsigned int final_result = 0; // TODO: find better name
	for (int i = 1; i < argc; i++) {

		// Output Formatting
		if (strcmp(argv[i], "-c")  == 0 || strcmp(argv[i], "c") == 0) {
			mode ^= COLOR;
		} else if (strcmp(argv[i], "-a") == 0 || strcmp(argv[i], "a") == 0) {
			mode ^= SHOW_ALL;
			printf("SHOW ALL\n");
		}
		
		// Tests to run
		else if (strcmp(argv[i], "-f") == 0 || strcmp(argv[i], "f") == 0) { // full test
			run_tests |= 0b1000;
		} else if (strcmp(argv[i], "-s") == 0 || strcmp(argv[i], "s") == 0) { // special test
			run_tests |= 0b0100;
		} else if (strcmp(argv[i], "-r") == 0 || strcmp(argv[i], "r") == 0) { // range test
			run_tests |= 0b0010;
		} else if (strcmp(argv[i], "-b") == 0 || strcmp(argv[i], "b") == 0) { // basic test
			run_tests |= 0b0001;
		}
		
		// methods to test
		else if (strcmp(argv[i], "-f2i") == 0 || strcmp(argv[i], "f2i") == 0) {
			to_test |= 0b001;
		} else if (strcmp(argv[i], "-i2f") == 0 || strcmp(argv[i], "i2f") == 0) {
			to_test |= 0b010;
		} else if (strcmp(argv[i], "-s2d") == 0 || strcmp(argv[i], "s2d") == 0) {
			to_test |= 0b100;
		} 
	}

	if (to_test == 0) {
		to_test = 0b111;
	}

	if (to_test & 0b001) { // if testing f2i
		if (run_tests == 0) { // run default set if none are specified
			run_tests |= 0b111;
		}
		final_result |= combine_tests(run_tests);
	}
	if (to_test & 0b010) { // if testing i2f
		if (run_tests == 0) {
			run_tests |= 0b111;
		}
		final_result |= combine_tests(run_tests) << 1;
	}
	if (to_test & 0b100) { // if testing s2d
		if (run_tests == 0) {
			run_tests |= 0b111;
		}
		final_result |= combine_tests(run_tests) << 2;
	}
	
	if (final_result != 0) {
		*tests = final_result;
	}
	return mode;
}

// runs all the tests
void test_runner(unsigned int tests, unsigned char mode) {
	unsigned long i2f_fail = 0;
	unsigned long f2i_fail = 0;
	unsigned long s2d_fail = 0;

	// Test: bits
	// 11: s2d full comprehensive test
	// 10: i2f full comprehensive test
	//  9: f2i full comprehensive test
	//  8: s2d special cases
	//  7: i2f special cases
	//  6: f2i special cases
	//  5: s2d range tests
	//  4: i2f range tests
	//  3: f2i range tests
	//  2: s2d basic tests
	//  1: i2f basic tests
	//  0: f2i basic tests

	// Comprehensive Tests
	if (tests & 0b001 << 9) {
		f2i_fail += f2i_full_test(mode);
	}
	if (tests & 0b010 << 9) {
		i2f_fail += i2f_full_test(mode);
	}
	if (tests & 0b100 << 9) {
		s2d_fail += s2d_full_test(mode);
	}

	// Special Case Tests
	if (tests & 0b001 << 6) {
		f2i_fail += f2i_infinities(mode);
		f2i_fail += f2i_zeros(mode);
		f2i_fail += f2i_large(mode);
	}
	if (tests & 0b010 << 6) {
		i2f_fail += i2f_truncate(mode);
		i2f_fail += i2f_round_to_even(mode);
		i2f_fail += i2f_add_one(mode);
	}
	if (tests & 0b100 << 6) {
		// TODO: implement s2d special cases
		s2d_fail += s2d_infinities(mode);
		s2d_fail += s2d_nan(mode);
		s2d_fail += s2d_denormalized(mode);
	}
	
	// Range Tests
	if (tests & 0b001 << 3) {
		f2i_fail += f2i_range(-100.5, 100.5, 0.3, mode);
	}
	if (tests & 0b010 << 3) {
		i2f_fail += i2f_range(-100, 100, 1, mode);
	}
	if (tests & 0b100 << 3) {
		s2d_fail += s2d_range(-100.5, 100.5, 0.3, mode);
	}

	// Basic Tests
	// TODO: add specific examples given in slides and class

	if (tests & 0b001001001001) {
		if (f2i_fail == 0) {
			fprintf(stderr, "f2i: All Tests Passed\n");
		} else {
			fprintf(stderr, "f2i: Failed %lu Tests\n", f2i_fail);
		}
	}
	if (tests & 0b010010010010) {
		if (i2f_fail == 0) {
			fprintf(stderr, "i2f: All Tests Passed\n");
		} else {
			fprintf(stderr, "i2f: Failed %lu Tests\n", i2f_fail);
		}
	}
	if (tests & 0b100100100100) {
		if (s2d_fail == 0) {
			fprintf(stderr, "s2d: All Tests Passed\n");
		} else {
			fprintf(stderr, "s2d: Failed %lu Tests\n", s2d_fail);
		}
	}
}

int main(int argc, char *argv[]) {
	// TODO: option parsing to run specific tests
	// TODO: overflow test
	// TODO: underflow test

	// Test: bits
	// 11: s2d full comprehensive test
	// 10: i2f full comprehensive test
	//  9: f2i full comprehensive test
	//  8: s2d special cases
	//  7: i2f special cases
	//  6: f2i special cases
	//  5: s2d range tests
	//  4: i2f range tests
	//  3: f2i range tests
	//  2: s2d basic tests
	//  1: i2f basic tests
	//  0: f2i basic test
	unsigned int tests = 0b000111111111; // Run all but full comprehensive tests by default
	unsigned char mode = parse_flags(argc, argv, &tests);

	test_runner(tests, mode);
}


