#!/bin/bash

# Make diff files
for file in 1 2 3; do
    ./utf8to16 "test/utf8ref$file.txt" "test/utf16BEtest$file.txt"
    if [ "$1" = "-l" ]; then
        ./utf8to16 "test/utf8ref$file.txt" "test/utf16LEtest$file.txt" $1
    fi
    ./utf16to8 "test/utf16BEref$file.txt" "test/utf8testBE$file.txt"
    ./utf16to8 "test/utf16LEref$file.txt" "test/utf8testLE$file.txt"
done

# make round trip tests
for file in 1 2 3; do
    ./utf8to16 "test/utf8testBE$file.txt" "test/utf16BEround$file.txt"
    if [ "$1" = "-l" ]; then
        ./utf8to16 "test/utf8testLE$file.txt" "test/utf16LEround$file.txt" $1
    fi
    ./utf16to8 "test/utf16BEtest$file.txt" "test/utf8roundBE$file.txt"
    if [ "$1" = "-l" ]; then
        ./utf16to8 "test/utf16LEtest$file.txt" "test/utf8roundLE$file.txt"
        ./utf8to16 "test/utf8testBE$file.txt" "test/utf16LEswap$file.txt" $1
    fi
    ./utf8to16 "test/utf8testLE$file.txt" "test/utf16BEswap$file.txt"
done

# Test for differences
for file in 1 2 3; do
    diff "test/utf8ref$file.txt" "test/utf8testBE$file.txt" && rm "test/utf8testBE$file.txt"
    diff "test/utf8ref$file.txt" "test/utf8testLE$file.txt" && rm "test/utf8testLE$file.txt"
    diff "test/utf8ref$file.txt" "test/utf8roundBE$file.txt" && rm "test/utf8roundBE$file.txt"
    if [ "$1" = "-l" ]; then
        diff "test/utf8ref$file.txt" "test/utf8roundLE$file.txt" && rm "test/utf8roundLE$file.txt"
    fi

    diff "test/utf16BEref$file.txt" "test/utf16BEtest$file.txt" && rm "test/utf16BEtest$file.txt"
    diff "test/utf16BEref$file.txt" "test/utf16BEround$file.txt" && rm "test/utf16BEround$file.txt"
    diff "test/utf16BEref$file.txt" "test/utf16BEswap$file.txt" && rm "test/utf16BEswap$file.txt"

    if [ "$1" = "-l" ]; then
        diff "test/utf16LEref$file.txt" "test/utf16LEtest$file.txt" && rm "test/utf16LEtest$file.txt"
        diff "test/utf16LEref$file.txt" "test/utf16LEround$file.txt" && rm "test/utf16LEround$file.txt"
        diff "test/utf16LEref$file.txt" "test/utf16LEswap$file.txt" && rm "test/utf16LEswap$file.txt"
    fi
done


# Error tests

printf "\nTesting Errors. Non-stderr output will be hidden.\n"

printf "\n-----------UTF 8 Tests-----------\n"
printf "\nExpect:\nMissing BOM ef bb b0\n"
./utf8to16 "test/utf8badbom.txt" /dev/null > /dev/null
printf "\nExpect:\nMissing or Unexpected Continuation byte at offset: 10\n"
./utf8to16 "test/utf8missingContinuation.txt" /dev/null > /dev/null
printf "\nExpect:\nMissing or Unexpected Continuation byte at offset: 7\n"
./utf8to16 "test/utf8unexpectedContinuation.txt" /dev/null > /dev/null
printf "\nExpect:\nOverlong Encoding at offset: 4\n"
./utf8to16 "test/utf8overlong.txt" /dev/null > /dev/null
printf "\nExpect:\nThe sequence decodes to a value reserved for surrogates: start byte at offset: 7\n"
./utf8to16 "test/utf8reservedSurrogate.txt" /dev/null > /dev/null
printf "\nExpect:\nThe sequence decodes to a value greater than 0x10FFFF: start byte at offset: 7\n"
./utf8to16 "test/utf8outofrange.txt" /dev/null > /dev/null
printf "\nExpect:\nThe sequence decodes to a noncharacter: start byte at offset: 7\n"
./utf8to16 "test/utf8noncharacter.txt" /dev/null > /dev/null
printf "\nExpect:\nMissing or unexpected continuation byte at offset: 24\n"
./utf8to16 "test/utf8missingContinuationEOF.txt" /dev/null > /dev/null

#Tests for All UTF-8 Non-Characters
for i in {64976..65007}
do
	byte0=$(( $(( 7 << 5 )) | $(( $i >> 12 )) ))
	byte1=$(( $(( 1 << 7 )) | $(( $(( $i >> 6 )) & 63 )) ))
	byte2=$(( $(( 1 << 7 )) | $(( $i & 63 )) ))
	printf "\nExpect:\nThe sequence decodes to a noncharacter: start byte at offset: 3\n"
	./utf8to16 <(printf '\xef\xbb\xbf'; printf "$(printf '\\x%x\\x%x\\x%x' $byte0 $byte1 $byte2)") /dev/null > /dev/null
done

for i in 65534 65535
do
	byte0=$(( $(( 7 << 5 )) | $(( $i >> 12 )) ))
	byte1=$(( $(( 1 << 7 )) | $(( $(( $i >> 6 )) & 63 )) ))
	byte2=$(( $(( 1 << 7 )) | $(( $i & 63 )) ))
	printf "\nExpect:\nThe sequence decodes to a noncharacter: start byte at offset: 3\n"
	./utf8to16 <(printf '\xef\xbb\xbf'; printf "$(printf '\\x%x\\x%x\\x%x' $byte0 $byte1 $byte2)") /dev/null > /dev/null
done

for i in {1..16}
do
	for j in 65534 65535
	do
		bytes=$(( $(( $i << 16 )) | $j ))
		byte0=$(( $(( 15 << 4 )) | $(( $bytes >> 18 )) ))
		byte1=$(( $(( 1 << 7 )) | $(( $(( $bytes >> 12 )) & 63 )) ))
		byte2=$(( $(( 1 << 7 )) | $(( $(( $bytes >> 6 )) & 63 )) ))
		byte3=$(( $(( 1 << 7 )) | $(( $bytes & 63 )) ))
		printf "\nExpect:\nThe sequence decodes to a noncharacter: start byte at offset: 3\n"
		./utf8to16 <(printf '\xef\xbb\xbf'; printf "$(printf '\\x%x\\x%x\\x%x\\x%x' $byte0 $byte1 $byte2 $byte3)") /dev/null > /dev/null
	done
done

printf "\n\n-----------UTF 16BE Tests-----------\n"
printf "\nExpect:\nMissing BOM 74 65\n"
./utf16to8 "test/utf16badbom.txt" /dev/null > /dev/null
printf "\nExpect:\nUnpaired surrogate at offset: 15\n"
./utf16to8 "test/utf16BEunpairedSurrogate.txt" /dev/null > /dev/null
printf "\nExpect:\nUnpaired surrogate at offset: 15\n"
./utf16to8 "test/utf16BEunpairedSurrogate2.txt" /dev/null > /dev/null
printf "\nExpect:\nIncomplete UTF-16 code unit at offset: 8\n"
./utf16to8 "test/utf16BEincompletecodeunit.txt" /dev/null > /dev/null
printf "\nExpect:\nThe sequence decodes to a noncharacter: start byte at offset: 11\n"
./utf16to8 "test/utf16BEnoncharacter.txt" /dev/null > /dev/null
printf "\nExpect:\nIncomplete UTF-16 code unit at offset: 2\n"
./utf16to8 "test/utf16BEincompleteCodeUnit2.txt" /dev/null > /dev/null

printf "\n\n-----------UTF 16LE Tests-----------\n"
printf "\nExpect:\nMissing BOM 74 65\n"
./utf16to8 "test/utf16badbom.txt" /dev/null > /dev/null
printf "\nExpect:\nUnpaired surrogate at offset: 15\n"
./utf16to8 "test/utf16LEunpairedSurrogate.txt" /dev/null > /dev/null
printf "\nExpect:\nUnpaired surrogate at offset: 15\n"
./utf16to8 "test/utf16LEunpairedSurrogate2.txt" /dev/null > /dev/null
printf "\nExpect:\nIncomplete UTF-16 code unit at offset: 6\n"
./utf16to8 "test/utf16LEincompletecodeunit.txt" /dev/null > /dev/null
printf "\nExpect:\nThe sequence decodes to a noncharacter: start byte at offset: 11\n"
./utf16to8 "test/utf16LEnoncharacter.txt" /dev/null > /dev/null
printf "\nExpect:\nIncomplete UTF-16 code unit at offset: 2\n"
./utf16to8 "test/utf16LEincompleteCodeUnit2.txt" /dev/null > /dev/null


printf "\n\nTODO: implement more noncharacter tests, there are only 66 total noncharacters\n"

exit 0
